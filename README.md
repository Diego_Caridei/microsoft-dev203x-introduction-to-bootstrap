# Microsoft: DEV203x Introduction to Bootstrap#

![edx-microsoft-logo.jpg](https://bitbucket.org/repo/9zoaE8/images/1239304987-edx-microsoft-logo.jpg)


#Module 0

Welcome

Pre-course survey

Getting around the course

You, the self-paced learner

No problem scores in this section

#Module 1 

Introducing Bootstrap

Page Layout

Labs 5 of 5 possible points (5/5) 100%

Quiz 4 of 4 possible points (4/4) 100%

GitHub Repository

#Module 2 

Page Design

Navigation

Enhanced Data Display

Lab: Enhanced Data Display 5 of 5 possible points (5/5) 100%

Quiz 5 of 5 possible points (5/5) 100%

#Module 3
Forms

Validation and Feedback

Lab: Validation and Feedback 5 of 5 possible points (5/5) 100%

Final Exam 11 of 11 possible points (11/11) 100%